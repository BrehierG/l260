'use strict';

const Joi = require('joi')

module.exports = {
  method: 'patch',
  path: '/film/{id}',
  options: {
    tags: ['api'],
    validate: {
      payload: Joi.object({
        titre: Joi.string().min(3).example('Le Hobbit').description('Nom du film'),
        description: Joi.string().min(3).example('Bilbon Sacquet vit une vie paisible dans la Comté jusqu\'à l\'arrivée de Gandalf').description('Description du film'),
        realisateur: Joi.string().min(3).example('JohnDoe').description('Realisateur du film'),
        releasedAt: Joi.date().example('12-12-2012'),
      }),
      params: Joi.object({
        id: Joi.string().required()
      })
    },
    auth: {
      scope: ['admin']
    }
  },
  handler: async (request, h) => {

    const { filmService } = request.services();
    const { userService } = request.services();
    const { mailerService } = request.services();

    const result = await filmService.edit(request.params.id, request.payload);

    const listeIdUser = await filmService.getUsersByFilmFavoris(request.params.id);
    listeIdUser.forEach(async elem => {
      const user = await userService.getUser(elem.user_id);
      await mailerService.sendMail(user[0],"Un de vos films favoris a été modifié")
      
    });

    return result;

  }
};
