'use strict';

const Joi = require('joi')

module.exports = {
  method: 'patch',
  path: '/user/{id}',
  options: {
    tags: ['api'],
    validate: {
      payload: Joi.object({
        firstName: Joi.string().required().min(3).example('').description('Firstname of the user'),
        lastName: Joi.string().required().min(3).example('').description('Lastname of the user'),
        username: Joi.string().min(3).example('').description('Username of the user'),
        mail: Joi.string().min(3).email().example('').description('email of the user'),
      }),
      params: Joi.object({
        id: Joi.string().required()
      })
    },
    auth: {
      scope: ['admin']
    }
  },
  handler: async (request, h) => {

    const { userService } = request.services();

    return await userService.edit(request.params.id, request.payload);
  }
};
