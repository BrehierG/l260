'use strict';
const Joi = require('joi')

module.exports = {
    method: 'post',
    path: '/film',
    options: {
        tags: ['api'],
        validate: {
            payload: Joi.object({
                titre: Joi.string().min(3).example('Le Hobbit').description('Nom du film'),
                description: Joi.string().min(3).example('Bilbon Sacquet vit une vie paisible dans la Comté jusqu\'à l\'arrivée de Gandalf').description('Description du film'),
                realisateur: Joi.string().min(3).example('JohnDoe').description('Realisateur du film'),
                releasedAt: Joi.date().example('12-12-2012'),
            })
        },
        auth: {
            scope: ['admin']
        }

    },
    handler: async (request, h) => {

        const { filmService } = request.services();
        const result = await filmService.create(request.payload);

        const { userService } = request.services();
        const { mailerService } = request.services();

        const listeUser = await userService.select();
        listeUser.forEach(async user => {
            await mailerService.sendMail(user,"Nouveau film disponible "+request.payload.titre);
        });
        return result;
    }
};
