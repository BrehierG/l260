'use strict';

const Joi = require('joi')

module.exports = {
  method: 'post',
  path: '/user',
  options: {
    tags: ['api'],
    validate: {
      payload: Joi.object({
        firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
        lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
        username: Joi.string().required().min(3).example('JohnDoe').description('Username of the user'),
        mail: Joi.string().required().min(3).email().example('john.doe@test.fr').description('email of the user'),
        password: Joi.string().required().min(8).description('password of the user'),
      })
    },
    auth: false
  },
  handler: async (request, h) => {

    const { userService } = request.services();
    const { mailerService } = request.services();

    const result =await userService.create(request.payload);
    await mailerService.sendMail(request.payload,"Bienvenue "+request.payload.firstName+" "+request.payload.lastName);
    return result;
  }
};
