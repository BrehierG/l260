'use strict';
const Boom = require('boom');
const Joi = require('joi')

module.exports = {
    method: 'post',
    path: '/add-favoris',
    options: {
        tags: ['api'],
        validate: {
            payload: Joi.object({
                film_id: Joi.number().integer().example(1).description('Film Id'),
            })
        },
        auth: {
            scope: ['user']
        }

    },
    handler: async (request, h) => {

        const { filmService } = request.services();
        const user_id = request.auth.credentials.id;
        const film_id = request.payload.film_id;
        const isFavoris = await filmService.isAlreadyFavoris({user_id,film_id});
        if(isFavoris){
            return Boom.notAcceptable("Déjà en favoris");
        }
        return await filmService.addFavoris({user_id,film_id});

    }
};
