'use strict';

const Joi = require('joi')
const Encrypt = require('@joffrey1323/iut-encrypt');
const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');

module.exports = {
  method: 'post',
  path: '/user/login',
  options: {
    tags: ['api'],
    validate: {
      payload: Joi.object({
        mail: Joi.string().min(3).email().required().description('email of the user'),
        password: Joi.string().min(8).required().description('password of the user')
      })
    },
    auth: false
  },
  handler: async (request, h) => {

    const { userService } = request.services();
    const result = await userService.login(request.payload.mail);

    const passwordEncrypt = result[0].password;
    console.log(result[0].scope);



    if (Encrypt.encryptInSHA1(request.payload.password) == passwordEncrypt) {

      const token = Jwt.token.generate(
        {
          aud: 'urn:audience:iut',
          iss: 'urn:issuer:iut',
          firstName: 'John',
          lastName: 'Doe',
          email: 'test@example.com',
          scope: result[0].scope,
          id: result[0].id
        },
        {
          key: 'random_string', // La clé qui est définit dans lib/auth/strategies/jwt.js
          algorithm: 'HS512'
        },
        {
          ttlSec: 14400 // 4 hours
        }
      );
      return token;
    } else {

      return Boom.unauthorized();
    }
  }
};
