'use strict';

const Joi = require('joi')

module.exports = {
    method: 'delete',
    path: '/user/{id}',
    config: {
        tags: ['api'],
        description: 'Remove specific user data',
        notes: 'Remove specific user data',
        validate: {
            params: Joi.object({
                id: Joi.string().required()
            })
        },
        auth : {
            scope: [ 'admin' ]
        }
    },
    handler: async (request, h) => {

        const { userService } = request.services();
        var del;
        await userService.delete(request.params.id).then(function(success){
            del = true;
        });
        if(del) return '';

        return "erreur";
    }
};
