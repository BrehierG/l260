'use strict';

const { Service } = require('schmervice');

module.exports = class FilmService extends Service {

    create(film) {

        const { Film } = this.server.models();

        return Film.query().insertAndFetch(film);
    }

    select() {
        const { Film } = this.server.models();

        // Objection retourne des promeses, il ne faut pas oublier des les await.
        return Film.query();

    }

    edit(id, data) {
        const { Film } = this.server.models();

        return Film.query().where('id', id).update(data);
    }

    addFavoris(data) {
        const { UserFilmFavoris } = this.server.models();
        return UserFilmFavoris.query().insertAndFetch(data);
    }

    delFavoris(data) {
        const { UserFilmFavoris } = this.server.models();
        return UserFilmFavoris.query().where({ 'user_id': data.user_id, 'film_id': data.film_id }).del();
    }

    async isAlreadyFavoris(data) {
        const { UserFilmFavoris } = this.server.models();
        const result = await UserFilmFavoris.query().where({ 'user_id': data.user_id, 'film_id': data.film_id });
        if (result.length == 0) {
            return false
        } else {
            return true;
        }
    }

    getUsersByFilmFavoris(id){
        const { UserFilmFavoris } = this.server.models();
        return UserFilmFavoris.query().where('film_id',id);
    }
}