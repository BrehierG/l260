'use strict';
const nodemailer = require('nodemailer');
const { Service } = require('schmervice');


module.exports = class MailerService extends Service {

   async sendMail(user, texte)
    {
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            secure: false,
            auth: {
                user: 'prince.renner62@ethereal.email',
                pass: '7S2x7epRUE2Yr7jkD5'
            }
        });
        
        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: '"Fred Foo 👻" <foo@example.com>', // sender address
            to: user.mail, // list of receivers
            subject: "Hello" + user.lastName + " " + user.firstName , // Subject line
            text: texte, // plain text body
            html: texte, // html body
        });

        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

}
