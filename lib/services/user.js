'use strict';

const { Service } = require('schmervice'); 

module.exports = class UserService extends Service {

        create(user){

             const { User } = this.server.models();
        
             return User.query().insertAndFetch(user);
        }
        
        select(){
            const { User } = this.server.models();
        
            // Objection retourne des promeses, il ne faut pas oublier des les await.
            return User.query();

        }

        delete(id){
            const { User } = this.server.models();

            return User.query().where('id',id).del();
        }

        edit(id, data){
            const { User } = this.server.models();

            return User.query().where('id',id).update(data);
        }

        login(data){
            const { User } = this.server.models();

            return User.query().where('mail',data);
        }

        getUser(id){
            const { User } = this.server.models();

            return User.query().where('id',id);
        }
}