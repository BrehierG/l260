'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');


module.exports = class UserFilmFavoris extends Model {

    static get tableName() {
        return 'user_films_favoris';
    }

    static get joiSchema() {

        return Joi.object({
           id: Joi.number().integer().greater(0),
           user_id: Joi.number().integer().example(1).description('User Id'),
           film_id: Joi.number().integer().example(1).description('Film Id'),
        });
    }


};
