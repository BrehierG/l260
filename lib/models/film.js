'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');
const Encrypt = require('@joffrey1323/iut-encrypt');

module.exports = class Film extends Model {

    static get tableName() {

        return 'film';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            titre: Joi.string().min(3).example('Le Hobbit').description('Nom du film'),
            description: Joi.string().min(3).example('Bilbon Sacquet vit une vie paisible dans la Comté jusqu\'à l\'arrivée de Gandalf').description('Description du film'),
            realisateur: Joi.string().min(3).example('JohnDoe').description('Realisateur du film'),
            releasedAt: Joi.date().example('12-12-2012'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    $beforeInsert(queryContext) {

        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};