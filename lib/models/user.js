'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');
const Encrypt = require('@joffrey1323/iut-encrypt');

module.exports = class User extends Model {

    static get tableName() {

        return 'user';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            firstName: Joi.string().min(3).example('John').description('Firstname of the user'),
            lastName: Joi.string().min(3).example('Doe').description('Lastname of the user'),
            username: Joi.string().min(3).example('JohnDoe').description('Username of the user'),
            mail: Joi.string().min(3).email().example('john.doe@test.fr').description('email of the user'),
            password: Joi.string().min(8).description('password of the user'),
            scope: Joi.string(),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    $beforeInsert(queryContext) {

        this.password = Encrypt.encryptInSHA1(this.password);
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
        this.scope = 'user';
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};