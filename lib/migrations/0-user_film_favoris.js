module.exports = {

    async up(knex) {

        await knex.schema.createTable('user_films_favoris', (table) => {
            table.increments('id').primary();
            table.integer('user_id').notNull();
            table.integer('film_id').notNull();

            table.foreign('user_id').references("id").inTable("user");
            table.foreign('film_id').references("id").inTable("film");

        });
    },

    async down(knex) {
        await knex.schema.dropTableIfExists('user_films_favoris');
    }
};
